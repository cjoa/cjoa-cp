# Introduction to Dynamic Programming

Una breve introducción a Programación Dinamica

---


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Introduction](#introduction)
- [Steps](#steps)
- [DP State](#dp-state)
- [DP Transitions](#dp-transitions)
- [Complexity](#complexity)
- [Code Templates](#code-templates)
- [Examples](#examples)


<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction

Dynamic Programming es un paradigma de diseño de algoritmos considerada "Smart Brute Force" o "Brute Force + memoización".  A diferencia de [Divide and Conquer](https://en.wikipedia.org/wiki/Divide-and-conquer_algorithm), en Dynamic Programming dividimos los problemas en [Overlappping Subproblems](https://en.wikipedia.org/wiki/Overlapping_subproblems): subproblemas más pequeños que se solapan entre sí (o sea, no son independientes).  La idea es que la cantidad de posibles subproblemas es pequeña y, gracias al solapamiento, se pueden reusar sus resultados para resolver los subproblemas más grandes usando el principio de [Optimal Substructure](https://en.wikipedia.org/wiki/Optimal_substructure). Este [artículo](http://20bits.com/article/introduction-to-dynamic-programming) explica bien estos conceptos de Overlapping Subproblems y Optimal Substructure.  En la practica, no tienes que conocer estos conceptos para aplicar esta técnica de Dynamic Programming.

Existen dos modos de trabajar con Dynamic Programming:
* **Top Down** o Recursivo 
* **Bottom Up** o Iterativo

Cuando usamos la forma recursiva, Dynamic Programming también se denomina [Memoization](https://en.wikipedia.org/wiki/Memoization).

Las tres clases de problemas que Dynamic Programming resuelve son:
* **Optimización**: minimizar un costo (o maximizar una ganancia) bajo ciertas restricciones
* **Combinatórica**: Contar todas las soluciones válidas
* **Decisión**: encontrar o determinar si existe alguna solución válida


## Steps

Para atacar problemas con Dynamic Programming de manera recursiva, se recomienda seguir los siguientes pasos:
1.  Identifica los **subproblemas** (o sea, sus parámetros, también llamado "estado de cada subproblema")
2.  Determina la **recurrencia** que relaciona los subproblemas (o sea, las transiciones entre subproblemas)
3.  Reconoce y resuelve los **casos bases**
4.  **Memoiza** la respuesta a cada subproblema
5.  Si el problema lo requiere, recupera (reconstruye) la solución óptima (asumiendo que es un problema de optimización o de decisión)

Este [artículo en freeCodeCamp](https://www.freecodecamp.org/news/demystifying-dynamic-programming-3efafb8d4296/) explica los primeros dos pasos.


## DP State

El estado de DP representa la situacion actual durante la ejecucion de la fuerza bruta.  Tambien representa el trabajo que falta por explorar en el brute force.

El estado del DP debe ser lo más compacto posible, ya que la complejidad de tu solucion dependera de la cantidad de estados posibles.  En consecuencia, el estado debe resumir todas las decisiones tomadas hasta ese momento, ignorando aquellos detalles que no son relevantes para la solución.

Los estados mas comunes son:
* Sufijo de un arreglo: el índice $`i`$ corresponde al intervalo $`[i, N)`$
* Prefijo de un arreglo: el índice $`i`$ representa el intervalo $`[0, i]`$
* Intervalo en una secuencia: índices $`L`$ y $`R`$ definen el intervalo $`[L, R]`$.
* Sub-matriz Inferior-Derecha en una matrix: los indices $`r`$ y $`c`$ corresponden a la sub-matrix desde $`[r, c]`$ hasta $`[N-1, M-1]`$
* Longitud
* Suma acumulado de algún recurso (monedas, tiempo, unidades de energía, etc.)
* Flag para indicar si un evento ocurrió
* Ultimo valor (o su índice) seleccionado

Para cada estado, asociamos el resultado que deseamos calcular:
* Para problemas de maximización (o minimización): asociamos la máxima ganancia (o mínimo costo) que se puede obtener a partir de ese estado
* Para problemas de contar: asociamos la cantidad de soluciones válidas desde ese estado hasta el final (caso base)
* Para problemas de decisión: el resultado es un booleano indicando si es posible obtener una solución válida a partir de ese estado


## DP Transitions

Las transiciones se derivan de las decisiones que se toman.  Por cada decisión, debes determinar cuáles son los parámetros del nuevo estado y como combinas el resultado asociado al nuevo estado.

Entre las decisiones mas comunes, tenemos el caso donde incluimos o excluimos un elemento de una secuencia.  Piensa en como el estado cambia si decides incluir el elemento y como cambia si decides excluirlo.


## Complexity

La complejidad de un algoritmo basado en Memoization es igual al producto de: 
1.  La cantidad de estados.  Esto es lo mismo que la cantidad de celdas de la tabla de memoización que se usa para recordar los resultados de los subproblemas
2.  La complejidad de la función recursiva (asumiendo que las llamadas recursivas toman O(1)).  Usualmente, esto es la cantidad de transiciones (por cada estado)


## Code Templates

### Maximize/Minimize

```python
def F(currentState) -> int:
    # base case
    if currentState is a base case:
        return -INF  # +INF for minimization
                     # or 0 depending on the problem

    if currentState is invalid:
        return -INF  # +INF for minimization

    # if already computed
    if cached[currentState]:
        return memo[currentState]

    # otherwise compute answer
    best = -INF  # +INF for minimization
    for each transition from currentState to nextState:
        best = max( best, gain( F(nextState) ) )
        # gain is some function that combines the recursive answer with current answer
        # replace max with min for minimization

    # store result for future calls
    cached[currentState] = true
    memo[currentState] = best

    return best
```


### Counting

```python
def F(currentState) -> int:
    # base case
    if currentState is a base case:
        return 1

    if currentState is invalid:
        return 0

    # if already computed
    if cached[currentState]:
        return memo[currentState]

    # otherwise compute answer
    res = 0
    for each transition from currentState to nextState:
        res += count( F(nextState) )
        # count is some function that combines the recursive answer with current answer

    # store result for future calls
    cached[currentState] = true
    memo[currentState] = res

    return res
```

### Decision

```python
def F(currentState) -> bool:
    # base case
    if currentState is a base case:
        return True

    if currentState is invalid:
        return False

    # if already computed
    if cached[currentState]:
        return memo[currentState]

    # otherwise compute answer
    res = False
    for each transition from currentState to nextState:
        res = logicalFunction( F(nextState) )
        # logicalFunction is some function that combines the recursive answer with current answer
        if res:
            break

    # store result for future calls
    cached[currentState] = true
    memo[currentState] = res

    return res
```


## Examples

La mejor manera de aprender Dynamic Programming es estudiando muchos ejemplos, empezando con los clásicos.

### Introductory Examples

* Fibonacci (para entender el árbol de recursión y el solapamiento de los subproblemas)
  * [Climbing Stairs](https://leetcode.com/problems/climbing-stairs/)
* [House Robber](https://leetcode.com/problems/house-robber/)
    <details>
    <summary>Solution</summary>

    ```cpp
    const int MAXN = 100005;  // maximo numero de casas + un margen por si acaso
    int N, A[MAXN];
    bool memo[MAXN];
    int memo[MAXN];
    int F(int n) {
       if (n >= N) return 0;  // caso base
       if (cached[n])      // precomputado anteriormente
          return memo[n];
       int res_robar = A[n] + F(n+2);  // resultado de robar en la n-esima casa
       int res_no_robar = F(n+1);      // resultado de ignorar la n-esima casa
       int res = max(res_robar, res_no_robar);  // tomar el maximo de ambos resultados
       cached[n] = true;    // marcar este estado como computado
       memo[n] = res;       // grabar resultado computado
       return res;
    }
    ```
    </details>

* Grid Paths
  * [Minimum/Maximum Path Sum in a 2D Grid](https://leetcode.com/problems/minimum-path-sum/)
  * [Counting Grid Paths](https://atcoder.jp/contests/dp/tasks/dp_h)


### Easy Examples
* [Removing Digits](https://cses.fi/problemset/task/1637)
* [Frog Jump](https://leetcode.com/problems/frog-jump/)
* [Frog 1](https://atcoder.jp/contests/dp/tasks/dp_a) y [Frog 2](https://atcoder.jp/contests/dp/tasks/dp_b)
* [Vacation](https://atcoder.jp/contests/dp/tasks/dp_c) y [Vacations](https://codeforces.com/problemset/problem/698/A)
* [Dance-Dance-Revolution](https://onlinejudge.org/index.php?option=onlinejudge&Itemid=8&page=show_problem&problem=4037)
* [Array Description](https://cses.fi/problemset/task/1746)



### Classic Examples

* Tipos "Knapsack"
  * [Subset Sum](https://leetcode.com/problems/partition-equal-subset-sum/)
  * [Min Coin Change](https://cses.fi/problemset/task/1634)
  * [(0-1) Knapsack](https://atcoder.jp/contests/dp/tasks/dp_d)
  * [Number of Ways to Make Change](https://cses.fi/problemset/task/1636)
* Secuencias
  * [Longest Increasing Subsequence](https://leetcode.com/problems/longest-increasing-subsequence/)
  * [Longest Common Subsequence](https://atcoder.jp/contests/dp/tasks/dp_f)
  * [Edit Distance](https://cses.fi/problemset/task/1639)


### Harder Examples
* [Longest Increasing Subquence](https://cses.fi/problemset/task/1145): Tiempo cuadratico no funciona
* [Weighted Interval Selection](https://cses.fi/problemset/task/1140)
* [Knapsack 2](https://atcoder.jp/contests/dp/tasks/dp_e)
* [Rectangle Cutting](https://cses.fi/problemset/task/1744)
* [Stones](https://atcoder.jp/contests/dp/tasks/dp_k): Game Theory
* [Removal Game](https://cses.fi/problemset/task/1097) o [Deque](https://atcoder.jp/contests/dp/tasks/dp_l)
* [Coins](https://atcoder.jp/contests/dp/tasks/dp_i): Calcular probabilidad de eventos


### Harder DP Techniques
* Bitmask DP:
  * [Elevator Rides](https://cses.fi/problemset/task/1653)
  * [Matching](https://atcoder.jp/contests/dp/tasks/dp_o)
* [Counting Numbers](https://cses.fi/problemset/task/2220): Digit DP
* [Sushi](https://atcoder.jp/contests/dp/tasks/dp_j): Calcular expected value
* Tree DP:
  * [Subordinates](https://cses.fi/problemset/task/1674)
  * [Tree Matching](https://cses.fi/problemset/task/1130)
  * [Indepedent Set](https://atcoder.jp/contests/dp/tasks/dp_p)


