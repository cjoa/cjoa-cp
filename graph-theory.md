## Definicion

Un grafo $`G = (V, E)`$ es una pareja ordenada donde $`V`$ es un conjunto de vertices (nodos) y $`E`$ es un conjunto de ejes (aristas) que relacionan parejas de vertices.

La teoria de grafo es una de las ramas de las matematicas que ha producido mayor cantidad de resultados fructiferos porque los grafos permiten modelar y/o simplificar el analisis de sistemas de la vida real: mapa de ciudades, arquitectura de red, diagrama de entidad-relacion en base de datos, relaciones entre personas, dependencias de objetos.

## Tipos de Grafos
Un grafo puede ser:
* Directed
* Undirected

Los ejes pueden tener pesos o no:
* Weighted
* Unweighted

Dependiendo si el grafo admite multiples ejes entre nodos y self-loops:
* Simple graph
* Multigraph

Dependiendo de la cantidad de ejes, un grafo puede ser:
* Dense
* Sparse

Dependiendo de la cantidad de componentes conexas:
* Connected
* Disconnected

### Grafos Especiales
* Complete: grafo simple que contiene todos los posibles ejes.
* Planar
* Tree
* Bipartite
* Directed Acyclic Graph (DAG)

## Terminología
* Self loop
* Degree
  * Para Directed Graph, In Degree / Out Degree
* Walk: puede ser Open (Path) o Closed (Cycle)
* Trail: walk sin ejes repetidos
* Circuit: closed trail (sin ejes repetidos)
* Simple Path: open walk sin repetición de ejes ni vertices
* Simple Cycle: closed walk sin repetición de ejes ni vertices
* Connected component
* Partial and Full Dynamic Connectivity
* Chromatic Number
* Graph Condensation
* Subgraph
* Clique

[Distincion entre Walk, Trail, Circuit, Path y Cycle](http://mathonline.wikidot.com/walks-trails-paths-cycles-and-circuits)

## Representacion de Grafos

[Tutorial en CS Academy](https://csacademy.com/lesson/graph_representation)

* Adjacency Matrix

```cpp
const int MAXN = 1000;
bool adj[MAXN][MAXN];
void add_edge(int u, int v) {
    adj[u][v] = true;
    adj[v][u] = true;  // comentar si el grafo es undirected
}
```

* Adjacency List

```cpp
const int MAXN = 100000;
vector<int> G[MAXN];
void add_edge(int u, int v) {
    G[u].push_back(v);
    G[v].push_back(u);  // comentar si el grafo es undirected
}
```


Para rooted trees, tambien existe la representacion con un arreglo llamado "Parent": cada nodo especifica cual es su padre.


## Problemas a estudiar
* Eulerian Trail/Circuit
* Hamiltonian Path/Cycle
* Graph Coloring
  * [Four-Color Theorem](https://en.wikipedia.org/wiki/Four_color_theorem)
* Exploracion de Grafos
  * DFS
    * Encontrar connected components
    * Two colorable
  * BFS
    * Shortest Path en unweighted graph
  * Complejidad $`O(V + E)`$ para adjacency list y $`O(V^2 )`$ para adjacency matrix
* Shortest Path
  * Single-Source Shortest Path
    * SPFA
    * Dijkstra
    * Bellman-Ford
  * All-Pairs Shortest Path
    * Floyd-Warshall
* Topological Sort
  * DFS
  * Kahn
* Conectividad
  * Deteccion de Ciclos
    * Undirected graph: visited array
    * Directed?
  * Bridges y Articulation Points
  * Union-Find
  * Strongly Connected Component
* Minimum Spanning Tree
  * Prim
  * Kruskal
* 2SAT
* Lowest-Common Ancestor
* Maximum Flow
* Maximum Bipartite Matching
* Maximum Weight Bipartite Matching
* Maximum General Matching
* Minimum Cut
* Max Independent Set
* Minimum Vertex Cover
* Graph Isomorphism


## Eulerian Trail / Circuit

Uno de los primeros problemas formulados en Graph Theory es el de los [Seven Bridges of Konigsberg](https://en.wikipedia.org/wiki/Seven_Bridges_of_K%C3%B6nigsberg).

Dado un grafo, encontrar un trail (o circuit) que visite cada edge exactamente una vez.

La existencia de Eulerian Trail (o Circuit) depende de los degrees de cada nodo.

* En un grafo undirected,
  * Existe Eulerian Circuit si cada nodo tiene degree par
  * Existe Eulerian Trail si no hay mas de dos nodos con degree impar.  Esos dos dos nodos con degree impar son los nodos inicios y finales del path.

* En un grafo directed,
  * Existe Eulerian Circuit si todos los nodos tienen $`inDegree = outDegree`$
  * Existe Eulerian Trail si todos los nodos tienen $`inDegree = outDegree`$, excepto quizas dos nodos donde uno de ellos, el origen, tiene $`inDegree = outDegree + 1`$ y el otro, el destino, tiene $`inDegree = outDegree - 1`$


## Hamiltonian Path / Cycle

Dado un grafo, encontrar un path (o cycle) que visite cada nodo exactamente una vez.

Desafortunadamente, no se conoce una solucion eficiente a este problema.

La solucion Brute Force consiste en listar todas las posibles permutaciones de los vertices y revisar si esa secuencia de vertices es valida: o sea, existe un edge entre cada pareja de nodos adyacentes en esa secuencia?


## Graph Exploration

Dos algoritmos de exploracion:
* Depth-First Search (DFS)
* Breadth-First Search (BFS)

```c++
enum State { UNVISITED, PROCESSING, VISITED };
int state[MAXN];
void dfs(int u) {
   state[u] = PROCESSING;
   for (int v : G[u]) {
      if (state[v] == UNVISITED)
         dfs(v);
   }
   state[u] = VISITED;
}
```

```c++
void bfs(int src) {
   queue<int> q;
   q.push(src);
   state[src] = PROCESSING;
   while (!q.empty()) {
      int u = q.front();
      q.pop();
      for (int v : G[u]) {
         if (state[v] == UNVISITED) {
            state[v] = PROCESSING;
            q.push(v);
         }
      }
      state[u] = VISITED;
   }
}
```

[Graph Traversals](http://www.cs.cornell.edu/courses/cs2112/2012sp/lectures/lec24/lec24-12sp.html)


## Shortest Path
Single-Source Shortest Path
  * Sin negative weight: Dijkstra, SPFA
  * Con Negative Weight: con Bellman-Ford

All-Pair Shortest Paths
  * Floyd-Warshall

Como recuperar un shortest path?

Aplicaciones de Shortest Path

Longest path
  * Longest Simple Path es NP Complete
  * Longest Non Simple Path: reducir a Shortest Path y resolver con Bellman-Ford
  * En un Directed Acyclic Graph
  * En un Tree

## Bicoloring
En general, el problema de encontrar la minima cantidad de colores requeridos para colorear los nodos de un grafo es dificil de resolver eficientemenre. Incluso, no existe un algoritmo eficiente para decidir si 3 colores son suficientes.

Sin embargo, determinar si un grafo es 2-colorable es sencillo, puesto que una vez decides el color inicial del nodo source en un componente, los demas nodos tienen sus colores forzados. Para esto, podemos implementar DFS o BFS.

## Topological Sort

En un directed graph, topological sort se refiere a encontrar un ordenamiento de los nodos de tal manera que si el nodo x aparece en la posicion i, todos sus antecedentes aparecen antes de la posicion i.

Existen dos algoritmos para resolver este problema:
* DFS
* Kahn

## Connectivity
* Static Connectivity
  * Undirected Graph
    * Bridge
    * Articulation point
    * 2-edge connected components
    * 2-vertex connected components (biconnected components)
  * Directed Graph
    * Strongly connected component
    * Kosaraju's Algorithm
      * Run DFS to find "topological ordering" of all nodes in graph G
      * GT = Transpose of Graph G
      * For each node in G (ordered by topological ordering):
           DFS(u) on graph GT
           Vertices discovered from u formed a strongly connected component
* Dynamic Connectivity
  * Partial Dynamic Connectivity
    * Incremental Connectivity
      * Union-Find
        * Disjoint Set
        * Union by Rank
        * Path Compression
    * Decremental Connectivity
  * Full Dynamic Connectivity

## Minimum-Spanning Tree

Dado un undirected connected graph, un Spanning Tree es un subgraph del graph que a la vez sea un arbol y que conected todos los nodos del grafo.  Un Minimum-Spanning Tree es aquel Spanning Tree de un undirected wegithed graph que minimiza la suma de los weights de los edges presentes en el arbol.

### Kruskal
Este algoritmo se basa en la idea de construir el arbol incrementalmente usando los edges en orden ascendente de sus weights.  Al intentar agregar un edge, verificamos si este edge crea un ciclo o no con los edges ya elegidos.  Esta deteccion de ciclos se puede hacer eficientemente con Disjoint Set.

Complejidad de Kruskal es $`O(E log E)`$

### Prim
Este algoritmo es muy similar al de Dijkstra, excepto que el source puede ser cualquier nodo arbitrario del grafo y los vertices pendientes a procesar estan ordenados por el weight mas pequenio de un edge que los une a los nodos ya procesados.

Complejidad de Prim es $`O((E+V) log V)`$

## Lowest Common Ancestor
Imaginense que queremos precomputar All-Pairs Distances en un Tree.  Si ejecutamos Floyd-Warshall, nos tomaremos $`O(V^3)`$ tiempo.  Pero ese algoritmo no aprovecha el hecho de que un Tree tiene pocos edges.  Si ejecutamos BFS (o DFS con DP), lograriamos tiempo de $`O(V * (V + E))`$ = $`O(V^2)`$.  Este tiempo es optimo porque existen $`V*(V-1)/2`$ parejas distintas de nodos.  Pero quizas solo nos interesan algunas parejas de nodos (no todas).  Nuestro objetivo es hacerlo en tiempo mejor que $`O(V^2)`$.  Nota que no podremos precomputar una tabla O(V ^ 2) porque eso tomaria todo el tiempo.

En un arbol, existe un solo camino entre cada pareja de vertices.  Que pasaria si "rooteamos" el arbol y precomputamos la distancia entre la raiz y cada nodo?  Entonces, la distancia entre dos nodos u y v es sencillo de calcular: este es igual a la distancia de la raiz al nodo u + la distancia de la raiz al nodo v, menos dos veces la distancia de la raiz al nodo donde la ruta se "bifurca".  Ese nodo es conocido como Lowest Common Ancestor de u y v.

Como computamos eficientemente el LCA de cada pareja de nodos u y v?
* Elegimos un root para el arbol
* Ejecutamos BFS (o DFS) desde ese root, calculando tres datos por cada nodo x:
  * Distancia desde la raiz hacia el nodo x
  * Profundidad del nodo x
  * El padre del nodo x
* Si depth(u) < depth(v), intercambiamos u y v
* Caminamos el nodo u hasta que se encuentre al mismo depth que v
* Caminamos ambos nodos (simultaneamente) hasta sus ancestros encontrar un nodo que es comun

La complejidad de ese algoritmo seria $`O(V)`$ worst-case.  Es posible mejorarlo?  Replantea el problema donde cada path fuera un linked link estatico.  Como podemos avanzar mas rapido?  Simplemente grabando informacion para tomar pasos mas grandes.  Resulta que es eficiente grabar el nodo que se encuentra a una distancia de una potencia de 2.
Si queremos avanzar K nodos, solo tenemos que reinterpretar K como una suma de potencias de 2 y tomar los saltos de cada potencia de 2. Esto resuelve la primera caminata.  Que pasa con la segunda?  Para esa, se puede hacer una especie de Binary Search: si intentas saltar muy lejos, ambos saltos caerian en el mismo nodo.  Si saltas muy cerca, ambos saltos terminan en nodos distintos.  Asi puedes intentar saltar por cada potencia de 2 de forma descencendente.


## Maximum Flow


## Resumen de Algoritmos

| Problema                      | Tipo de Grafo           | Algoritmo          | Complejidad  |
| ----------------------------- | ----------------------- | ------------------ | ------------ |
| Eulerian Path/Circuit         | Any                     | Hierholzer         | $`E`$            |
| Hamiltonian Path/Cycle        | Any                     | Brute Force        | $`V! * V`$       |
|                               | Any                     | DP                 | $`2^V * V`$       |
| Graph Exploration             | Any                     | DFS                | $`V + E`$        |
|                               | Any                     | BFS                | $`V + E`$        |
| Two-Colorable                 | Undirected              | DFS                | $`V + E`$        |
| All-Pair Shortest Path        | Any (matrix)            | Floyd-Warshall     | $`V^3`$          |
| Single-Source Shortest Path   | (non-negative) Weighted | Dijkstra           | $`(V + E) * log V`$  |
|                               | Weighted                | Bellman-Ford       | $`V * E`$           |
| Longest (non-simple) path     | Directed                | Bellman-Ford       | $`V + E`$        |
|                               | Tree                    | DFS                | $`V + E`$        |
| Articulation points           | Undirected              | Tarjan's DFS       | $`V + E`$        |
| Bridges                       | Undirected              | Tarjan's DFS       | $`V + E`$        |
| Union-Find                    | Undirected              | Disjoint Set       | $`\alpha(V)`$ avg. por operacion |
| Minimum Spanning Tree         | Undirected & weighted   | Kruskal            | $`V + E * log E`$  |
|                               |                         | Prim               | $`(V + E) * log V`$  |
| Topological Sort              | DAG                     | DFS                | $`V + E`$        |
|                               |                         | Kahn               | $`V + E`$        |
| 2-vertex connected components | Undirected              | Tarjan's DFS       | $`V + E`$        |
| 2-edge connected components   | Undirected              | Tarjan's DFS       | $`V + E`$        |
| Strongly Connected Components | Directed                | Tarjan             | $`V + E`$        |
|                               | Directed                | Kosaraju           | $`V + E`$        |
| 2SAT                          | Implication Graph       | Reduce to SCC + TopSort | $`V + E`$ |
| Lowest Common Ancestor        | Tree                    | Binary Lifting     | $`log V`$ por query |
| Maximum Flow                  | w/Capacity              | Ford-Fulkerson     | $`E * f`$        |
|                               |                         | Edmonds-Karp       | $`E^2 * V`$      |
|                               |                         | Dinic              | $`E * V^2`$      |
| Max Bipartite Matching        | Bipartite               | Ford-Fulkerson     | $`E * V`$        |
|                               |                         | Hopcroft-Karp      | $`E * sqrt(V)`$  |
| Assignment                    | Weighted                | Hungarian Method (Munkres) | $`V^3`$        |
| General Matching              | Undirected              | Edmonds            | $`E * V^2`$      |
| Max Independent Set           | Any                     | Brute Force        | $`2^V * V^2`$    |
| Max Clique                    | Any                     | Reduce to MIS      | $`2^V * V^2`$    |
| Min Vertex Cover              | Any                     | Reduce to MIS      | $`2^V * V^2`$    |
|                               | Tree                    | DFS + Greedy       | $`V + E`$        |
|                               | Bipartite Graph         | Reduce to MBM      | $`E * V`$        |
| Min Edge Cover                | Undirected              | Matching + Greedy  | $`E * V^2`$      |




