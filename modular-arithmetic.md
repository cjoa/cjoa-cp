# Modular Arithmetic

Primero, definamos la operación de módulo:
* Dado un numero entero **_a_** y un entero positivo **_M_**, $`a \pmod{M}`$ es el residuo de dividir **_a_** por **_M_**.

Matemáticamente, esta operación produce un número entero en el rango de **_0_** a **_M-1_**.  En la mayoría de los lenguajes de programación, esta operación es implementada utilizando el operador %.  OJO: en la gran mayoría de los lenguajes de programación, si **_a_** es negativo, este operador devuelve un número negativo.

La aritmética modular es un conjunto de operaciones matemáticas que trabajan sobre números enteros, donde los números "dan la vuelta" si se pasan de un valor fijo **_M_** o se vuelven negativos.  En este sistema, todos los números enteros que al dividirse por **_M_** resultan en el mismo residuo son considerados equivalentes (el término matemático correcto es que son _congruentes_).

$`x \equiv y \pmod{M}`$

quiere decir que **_x_** y **_y_** dejan el mismo residuo al dividirse por **_M_**.

Nota que $`M \; | \; (x - y)`$.

Tener en cuenta que **_x_** y **_y_** pueden ser números negativos para .  Por ejemplo:

* $`-3 \equiv 7 \pmod{10}`$
* $`9 \equiv -1 \pmod{10}`$
* $`-5 \equiv -15 \pmod{10}`$


Otros tutoriales:
* [Art of Problem Solving](https://artofproblemsolving.com/wiki/index.php/Modular_arithmetic/Introduction)
* [Brilliant](https://brilliant.org/wiki/modular-arithmetic/)
* [Codeforces](https://codeforces.com/blog/entry/72527)


## Sum

```python
a = 3489045645
b = 6243534907
M = 10
(a + b) % M == ((a % M) + (b % M)) % M
```

### Ejercicios de Practica
1. [Merge it!](https://codeforces.com/contest/1176/problem/B)



## Product


```python
a = 3489045645
b = 6243534907
M = 10
(a * b) % M == ((a % M) * (b % M)) % M
```


### Ejercicios de Practica
1. [https://toph.co/p/big-factorials](Big Factorials)



## Substraction


```python
a = 3489045645
b = 6243534907
M = 10
(a - b) % M == ((a % M) - (b % M) + M) % M
```


## Power

```python
a = 6243534907
n = 123
M = 10
(a ** n) % M == ((a % M) ** n) % M
```

Nota que no se aplica la operación mod **_M_** al exponente **_n_**.  Pero si es posible "reducir" el exponente **_n_** utilizando [Euler's Theorem](https://en.wikipedia.org/wiki/Euler%27s_theorem), el cual dice:

$`a^{\phi(M)} \equiv 1 \pmod{M}`$

donde:

* $`\phi(M)`$ es el [Euler's Totient function](https://en.wikipedia.org/wiki/Euler%27s_totient_function): la cantidad de números enteros positivos no mayores que **_M_** que son co-primos de **_M_**.


```c++
int phi(int x) {
   int res = x;
   for (int i = 2; i * i <= x; ++i) {
      if (x % i == 0) {
         res -= res / i;
         do {
            n /= i;
         } while (x % i == 0);
      }
   }
   if (x > 1)
      res -= res / x;
   return res;
}
```

Complejidad: $`O(\sqrt{M})`$.



### How to compute modular power

El método obvio de computar $`a^n \pmod{M}`$ es convertir la potencia en un producto (donde aplicamos la regla del producto):

$`a^n = a \cdot a \cdot ... \cdot a \pmod{M}`$ donde el termino **_a_** aparece **_n_** veces en ese producto.

¿Existe un método más eficiente? Sí: el algoritmo llamado _Exponentiation by Squaring_ se basa en que la idea de que:
* Si **_n_** es par, $`a^n = (a^{2(n/2)}) = (a^2)^{n/2}`$.
* Si **_n_** es impar, podemos convertirlo a par, extrayendo un factor **_a_**: $`a^n = a^{n-1} \cdot a`$.


Recursivo:
```cpp
unsigned int powmod(unsigned long long a, unsigned long long n, unsigned int M) {
   a %= M;   // b puede exceder de M (32-bits)
   if (n == 0)
      return 1 % M;
   if ((n % 2) == 0)
      return powmod(a*a, n/2, M);
   else
      return (powmod(a*a, n/2, M) * a) % M;
}
```


Iterativo: basado en pseudocódigo tomado de [wikipedia](https://en.wikipedia.org/wiki/Modular_exponentiation#Pseudocode)
```cpp
unsigned int powmod(unsigned long long a, unsigned long long n, unsigned int M) {
   if (M == 1) return 0;
   unsigned int res = 1;
   a %= M;
   for (; n > 0; n /= 2) {
      if ((n % 2) == 1)
         res = res * a % M;
      a = a * a % M;
   }
   return res;
}
```


### Ejercicio de Practica
1. [Exponentiation](https://cses.fi/problemset/task/1095)
2. [Rhezo And Big Powers](https://www.hackerearth.com/practice/math/number-theory/basic-number-theory-1/practice-problems/algorithm/rhezo-and-big-powers-1/)
3. [Exponentiation II](https://cses.fi/problemset/task/1712)


## Division

No es posible aplicar una regla similar como en las operaciones anteriores de suma, multiplicación y resta.  Por ejemplo:

```c++
#include <iostream>
int main() {
    int a = 16;
    int b = 2;
    int M = 10;
    
    std::cout << (a / b) % M << std::endl;
    std::cout << ((a % M) / (b % M)) % M << std::endl;
}
```

Para dividir **_a_** entre **_b_** bajo módulo **_M_**, necesitamos calcular el inverso modular de **_b_** bajo módulo **_M_** y luego multiplicar el inverso modular por **_a_**.  Esencialmente, convertimos la división en una multiplicación.

El inverso modular de **_b_** bajo módulo **_M_** es un número entero (entre **_1_** y **_M-1_**) que multiplicado por **_b_** resulta en el valor _1_ (trabajando bajo módulo **_M_**).  El inverso modular de **_b_**, escrito como $`b^{-1}`$ es único en el rango entre **_1_** y **_M-1_**.

Por ejemplo, el inverso modular de **_3_** bajo módulo **_10_** es **_7_**, porque $`7 \times 3 = 21`$, lo cual es equivalente a **_1_** (bajo módulo **_10_**).

El inverso modular no siempre existe.  Dos ejemplos:
* Ejemplo trivial: si **_b_** es divisible por **_M_**, entonces $`b \equiv 0 \pmod{M}`$. Entonces, $`b{-1} \equiv 0^{-1} \mod{M}`$.

* Bajo módulo **_6_**, no existe un inverso modular para **_2_**.

Resulta que la condición suficiente para que exista es que **_b_** y **_M_** sean co-primos (o sea, no tengan ningún factor común mayor que **_1_**).  Es por eso que en muchos ejercicios de programacion competitiva, **_M_** es un número primo (para permitirte dividir si lo requieres).


Tenemos tres maneras eficientes de obtener el inverso modular:
* [Fermat's Little Theorem](#fermat-s-little-theorem)
* [Extended Euclidean Algorithm](#extended-euclidean-algorithm)
* [Euclidean Division](#euclidean-division)

### Fermat's Little Theorem

Si el módulo **_M_** es un número primo (y **_a_** no es divisible entre **_M_**), podemos usar [Fermat's Little Theorem](https://en.wikipedia.org/wiki/Fermat%27s_little_theorem), el cual dice que $`a^{p-1} \equiv 1 \pmod{p}`$ si **_p_** es primo (y **_a_** no es divisible entre **_p_**).  En ese caso, podemos "dividir" ambos lados de la ecuación por **_a_** para obtener la igualdad $`a^{p-2} = a^{-1}`$.

```cpp
unsigned int mod_inverse(unsigned int a, unsigned int M) {
   return powmod(a, M-2, M);
}
```

Complejidad: $`O(log(M))`$


### Extended Euclidean Algorithm

Otra alternativa es usar el algoritmo extendido de Euclides de la sección [Euclidean Algorithm](number-theory.md#euclidean-algorithm) porque Bezout's Identity establece que:

$`a \times x + M \times y = gcd(a, M)`$

Si **_a_** y **_M_** son co-primos (o sea, $`gcd(a, M) = 1`$), esa identidad se simplifica a:

$`a \times x + M \times y = 1`$

Lo cual quiere decir que **_x_**, por definición, es el inverso modular de **_a_**.

```cpp
int x, y, d;
int d = egcd(a, M, x, y);
assert(d == 1);  // asegurarnos de que a y M sean coprimos
if (x < 0)   // x puede ser negativo, en cuyo caso le sumamos M
   x += M;
return x;
```

Complejidad: $`O(log(min(a, M))) = O(log(a))`$


### Euclidean Division

```c++
int mod_inverse(int a, int M) {
   if (a <= 1)
      return a;
   return M - (M / x) * mod_inverse(M % x, M) % M;
}
```

Este algoritmo se puede utilizar para precomputar todos los inversos modulares debajo de **_M_**:

```c++
vector<int> get_mod_inverses(int M) {
   vector<int> inv(M);
   inv[1] = 1;
   for (int i = 2; i < M; i++)
      inv[i] = M - M / i * inv[M % i] % M;
   return inv;
}
```


## Ejercicio de práctica:
1. [Santa's Bot](https://codeforces.com/contest/1279/problem/D)

---

