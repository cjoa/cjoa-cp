# Segment Tree

Una breve introducción a Segment Tree para responder Range Queries

---


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Introduction](#introduction)
- [Static](#static)
- [Point Updates](#point-updates)
- [Range Updates, Point Queries](#range-updates-and-queries)
- [Range Updates And Queries](#range-updates-and-queries)
- [MergeSort Tree](#mergesort-tree)


<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Introduction
Segment Tree es una estructura de datos que permite contestar Range Queries sobre un arreglo _A_ de manera eficiente (usualmente en tiempo $`O(log N)`$).  Esta estructura consiste en construir un (Almost) Complete Binary Tree que nos permite subdividir el arreglo _A_ en intervalos de tamaño N, N/2, N/4, N/8, etc., hasta llegar al tamaño 1.  Cada nodo del árbol es responsable de uno de esos intervalos.

Por ser un Complete Binary Tree, no es necesario construir explícitamente el árbol usando punteros, sino que usaremos un simple arreglo para representarlo.  Este arreglo, _T_, requiere de una capacidad de $`4 \times N`$ (o más precisamente $`2 \times 2^{\lceil log_2(N) \rceil} )`$ celdas.  Para simplificar la artimética, no usaremos la posición 0 del arreglo _T_. Los nodos del árbol se identifican por su índice en el arreglo, donde la raiz se ubica en la posición 1. Para el nodo _u_:
* $`2 \times u`$ es el índice del hijo izquierdo
* $`2 \times u + 1`$ es el índice del hijo derecho
* $`\lfloor u / 2 \rfloor`$ es el padre (si _u_ no es la raíz)

En nuestra implementación, los intervalos [_L_, _R_] del arreglo _A_ son:
* 0-based
* cerrados, es decir, los índices _L_ y _R_ están incluidos en el intervalo. 

Algunas de las operaciones de rangos que Segment Tree puede contestar son: suma, mínimo, máximo, gcd, producto, etc.  En nuestras implementaciones debajo, asumimos que la operación es suma, pero es sencillo de reemplazar por la función que desees.  Para este fin, definimos el tipo de dato genérico que vamos a grabar en el Segment Tree y una función llamada _combine(a, b)_ que es la que especifica el tipo de operacion que queremos computar.

```cpp
using Node = long long;
const Node NEUTRAL_VALUE = 0;
Node combine(Node a, Node b) {
    return a + b;
}
```


## Static

Esta es la versión que no contiene operaciones para actualizar el Segment Tree.  Nota que para suma de rangos, es mucho más simple implementar Prefix Sum que Segment Tree.  Sin embargo, para otros tipos de queries como Min y/o Max, no es posible usar Prefix Array.


### Version Recursiva
```cpp
struct SegmentTree {
    int M;
    vector<Node> T;  // este es el complete Binary Tree representado en un arreglo

    void build(const vector<int>& A, int u, int tl, int tr) {
        if (tl == tr) {
            T[u] = A[tl];
            return;
        }
        int mid = (tl + tr) / 2;
        build(A, 2*u, tl, mid);
        build(A, 2*u+1, mid+1, tr);
        T[u] = combine(T[2*u], T[2*u+1]);
    }

    SegmentTree(const vector<int>& A) : M(A.size()) {
        int N = 1;
        while (N < (int) A.size()) N *= 2;
        T.assign(2*N, NEUTRAL_VALUE);
        build(A, 1, 0, M-1);
    }

    Node query(int ql, int qr, int u, int tl, int tr) {
        if (qr < tl || ql > tr) return NEUTRAL_VALUE;  // intervalos no solapan
        if (ql <= tl && tr <= qr) return T[u];         // contenido dentro
        int mid = (tl + tr) / 2;
        return combine(query(ql, qr, 2*u, tl, mid),
                       query(ql, qr, 2*u+1, mid+1, tr));
    }

    Node query(int ql, int qr) {
      return query(ql, qr, 1, 0, M-1);
    }
};
```


### Version Iterativa
```cpp
struct SegmentTree {
    int N;           // proxima potencia de 2 mayor o igual que el tamanio del arreglo
    vector<Node> T;  // este es el complete Binary Tree representado en un arreglo

    void build(const vector<int>& A) {
        for (int i = 0; i < A.size(); i++)
            T[i + N] = A[i];
        for (int u = N-1; u > 0; --u)
            T[u] = combine(T[2*u], T[2*u+1]);
    }

    SegmentTree(const vector<int>& A) {
        N = 1;
        while (N < (int) A.size()) N *= 2;
        T.assign(2*N, NEUTRAL_VALUE);
        build(A);
    }

    Node query(int ql, int qr) {
        Node resL = NEUTRAL_VALUE, resR = NEUTRAL_VALUE;
        for (int l = ql + N, r = qr + N, l <= r; l /= 2, r /= 2) {
            if ((l % 2) == 1) resL = combine(resL, T[l++]);
            if ((r % 2) == 0) resR = combine(T[r--], resR);
        }
        return combine(resL, resR);
    }
};

```

### Ejercicio(s) de Practica

* [Static Range Minimum Queries](https://cses.fi/problemset/task/1647)
* [Frequent Values](https://onlinejudge.org/external/112/11235.pdf)


## Point Updates
Las actualizaciones se restringen a una sola celda por operación.  Usamos recursión para llegar al leaf que contiene el dato a actualizar.  Cuando la recursión retorna, actualizamos el padre con los resultados de sus dos hijos.

### Version Recursiva
```cpp
    void update(int p, Node value, int u, int tl, int tr) {
        if (p < tl || p > tr) return;    // posicion p fuera de intervalo
        if (tl == tr) {      // llegamos a un leaf
            T[u] = value;
            return;
        }
        int mid = (tl + tr) / 2;
        update(p, val, 2*u, tl, mid);
        update(p, val, 2*u+1, mid+1, tr);
        T[u] = combine(T[2*u], T[2*u+1]);
    }
```

### Version Iterativa
```cpp
    void update(int p, Node value) {
        int u = p + N;
        T[u] = value;
        for (u /= 2; u > 0; u /= 2)
            T[u] = combine(T[2*u], T[2*u+1]);
    }
```

### Ejercicio(s) de Practica
* [Dynamic Range Minimum Queries](https://cses.fi/problemset/task/1649)
* [Help Ashu](https://www.hackerearth.com/practice/data-structures/advanced-data-structures/fenwick-binary-indexed-trees/practice-problems/algorithm/help-ashu-1/)
* [List Removals](https://cses.fi/problemset/task/1749)
* [Salary Queries](https://cses.fi/problemset/task/1144/)
* [Hotel Queries](https://cses.fi/problemset/task/1143)
* [Order statistic set](https://www.spoj.com/problems/ORDERSET/en/)
* [Inversions](http://codeforces.com/edu/course/2/lesson/4/3/practice/contest/274545/problem/A)
* [Distinct Value Queries](https://cses.fi/problemset/task/1734)
* [Prefix Sum Queries](https://cses.fi/problemset/task/2166/)
* [Subarray Sum Queries](https://cses.fi/problemset/task/1190)
* [Rent your airplane and make money](https://www.spoj.com/problems/RENT/)


## Range Updates and Point Queries
Aquí los queries son de una posición específica. Las posibles operaciones son:
1. INCREASE _L_ _R_ _v_: Incrementa todas las celdas en el rango de _L_ a _R_ por el valor _v_
2. QUERY _p_ : Imprime el valor grabado en la posición _p_

Este se puede resolver con el truco de _deltas_: en los updates, agregamos el valor _v_ en la posición _L_ y substraemos el valor _v_ de la posicion _R_ + 1.  La respuesta a cada operación QUERY equivale a la suma en el rango [1, _p_] (one-based).

Esto funciona porque el Segment Tree se construye en base al arreglo de _diferencias_ _D_ del arreglo input _A_, donde _D[i]_ = _A[i]_ - _A[i-1]_.  Por ejemplo, si _A_ = [x, 3, 7, 1, 4, 2, 2], el arreglo de diferencias es [x, 3, 4, -6, 3, -2, 0].  Nota que se asume que el indice 0 del arreglo _A_ no se usa, pero se asume que tiene el valor 0.  En cualquier instante, el arreglo _A_ se puede recuperar calculando el prefix sum del arreglo de diferencias.  Si incrementamos 4 unidades a las celdas desde las posiciones 3 a 5, el arreglo de diferencias cambia solamente en las posiciones 3 y 6 (0-based): [3, 4, -2, 3, -2, -4].  


### Ejercicio(s) de Practica
* [Range Update Queries](https://cses.fi/problemset/task/1651)



## Range Updates and Queries
Este requiere una técnica llamada Lazy Propagation: la idea es retener cada update en los nodos superiores y propagar esos updates solamente cuando accedemos a un nodo inferior.  Para ello, creamos atributos en el nodo que graben esos updates pendientes cuando el intervalo de busqueda contiene el intervalo bajo la responsabilidad del nodo.

En nuestra implementación, agregamos dos arreglos adicionales (que corresponden a los atributos de cada nodo):
* lazy: es el valor a propagar a sus hijos
* has_lazy: es un flag que indica si tiene o no un update pendiente a propagar a sus hijos

```cpp
    vector<int> lazy;
    vector<bool> has_lazy;
    void apply(int value, int u, int tl, int tr) {
        has_lazy[u] = true;
        lazy[u] = value;
        T[u] = value*1LL*(tr - tr + 1);
    }
    void push(int u, int tl, int tr) {
        if (has_lazy[u]) {
            int mid = (tl + tr) / 2;
            apply(lazy[u], 2*u, tl, mid);
            apply(lazy[u], 2*u+1, mid+1, tr);
            has_lazy[u] = false;
        }
    }
    void update(int ql, int qr, int value, int u, int tl, int tr) {
        if (qr < tl || ql > tr) return;    // intervalo [ql, qr] no intersecta [tl, tr]
        if (ql <= tl && tr <= qr) {        // intervalo [ql, qr] contiene a [tl, tr]
            apply(value, u, tl, tr);
            return;
        }
        push(int u, int tl, int tr);
        int mid = (tl + tr) / 2;
        update(p, 2*u, tl, mid);
        update(p, 2*u+1, mid+1, tr);
        T[u] = combine(T[2*u], T[2*u+1]);
    }

    Node query(int ql, int qr, int u, int tl, int tr) {
        if (qr < tl || ql > tr) return NEUTRAL_VALUE;  // intervalos no solapan
        if (ql <= tl && tr <= qr) return T[u];         // contenido dentro
        push(int u, int tl, int tr);
        int mid = (tl + tr) / 2;
        return combine(query(ql, qr, 2*u, tl, mid),
                       query(ql, qr, 2*u+1, mid+1, tr));
    }
```

### Ejercicio(s) de Practica
* [Range Updates and Sums](https://cses.fi/problemset/task/1735)
* [Multiples of 3](https://www.codechef.com/problems/MULTQ3)
* [Ahoy, Pirates!](https://onlinejudge.org/external/114/11402.pdf)


## MergeSort Tree

### Ejercicio(s) de Practica
* [K-query](https://www.spoj.com/problems/KQUERY/)


## Persistent Segment Tree




