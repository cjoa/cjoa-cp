# Competitive Programming Topics

Breves explicaciones de varios temas en Programación Competitiva

---


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Number Theory](number-theory.md)
- [Modular Arithmetic](modular-arithmetic.md)
- [Combinatorics](combinatorics.md)
- [Geometry](geometry)
- [Segment Tree](segment-tree.md)
- [Dynamic Programming](dynamic-programming.md)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


---
