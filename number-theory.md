# Introduction to Number Theory

Una breve introducción a Teoría de Numeros para Programación Competitiva

---


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Divisors](#divisors)
- [Prime Numbers](#prime-numbers)
- [GCD](#gcd)
- [LCM](#lcm)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


# Divisors
Un divisor (o factor) de un número entero **_x_** es un número entero **_d_** que divide exactamente a **_x_** (sin dejar residuos). Matemáticamente, se dice que **_d_** divide a **_x_** y se escribe $`d \; | \; x`$.  Si **_d_** no es un divisor, es escribe $`d \not| \;x`$.  Si **_d_** es divisor de **_x_**, se dice que **_x_** es un múltiplo de **_d_**.


## Divisibility Rules

https://en.wikipedia.org/wiki/Divisibility_rule



## How to obtain all divisors of a number **_x_**?

### Trial division upto **_x_**
El algoritmo de fuerza bruta consiste en iterar **_d_** de 1 a **_x_**: verificar si **_d_** divide (sin residuos) a **_x_** o no.

```cpp
vector<long long> get_divisors(long long x) {
   vector<long long> divisors;
   for (long long d = 1; d <= x; ++d) {
      if (x % d == 0)
         divisors.push_back(d);
   }
   return divisors;
}
```
Complejidad: $`O(x)`$

### Trial division upto $`\sqrt{x}`$
Si **_d_** es un divisor de **_x_**, **_x/d_** es otro divisor (a menos que **_d_** = **_x/d_**).  Uno de ellos debe ser menor o igual que la raíz cuadrada de **_x_** (y el otro mayor o igual). En consecuencia, solo necesitamos iterar hasta la raíz cuadrada de **_x_**.

Esto también explica la razón por la cual si un número entero no negativo **_x_** tienen una cantidad impar de divisores, **_x_** es un cuadrado perfecto. Y si el número tiene una cantidad par de divisores, no es cuadrado perfecto. 

```cpp
vector<long long> get_divisors(long long x) {
   vector<long long> divisors;
   for (long long d = 1; d*d <= x; ++d) {
      if (x % d == 0) {
         divisors.push_back(d);
         long long od = x / d;
         if (od != d)
            divisors.push_back(od);
      }
   }
   return divisors;
}
```
Complejidad: $`O(\sqrt{x})`$


## Divisor Count
Se estima que la máxima cantidad de divisores de un número entero hasta **_x_** es $`O(\sqrt[3]{x})`$.  Referencia: https://codeforces.com/blog/entry/22317.

Es posible calcular la cantidad de divisores en complejidad de $`O(\sqrt[3]{x})`$, pero el [algoritmo](https://codeforces.com/blog/entry/22317) está fuera del ámbito de esta introducción.



### Highly Composite Numbers
Dada la estimación anterior de $`O(\sqrt[3]{x})`$ para el _peor_ caso, resulta que la cantidad de divisores es relativamente pequeña:

|      x      |  # with max divisor count | divisor count |
|------------:|--------------------------:|--------------:|
| $`10^5`$    |              83160        |      128      |
| $`10^6`$    |             720720        |      240      |
| $`10^7`$    |            8648640        |      448      |
| $`10^8`$    |           73513440        |      768      |
| $`10^9`$    |          735134400        |     1344      |
| $`10^{10}`$ |         6983776800        |     2304      |
| $`10^{12}`$ |       963761198400        |     6720      |


Para mas información, consultar:
https://en.wikipedia.org/wiki/Highly_composite_number
http://wwwhomes.uni-bielefeld.de/achim/highly.txt


## Ejercicios de práctica:
1. [Common Divisors](https://codeforces.com/contest/1203/problem/C)  TLE porque los constraints estan ligeramente altos.
2. [Not Divisible](https://atcoder.jp/contests/abc170/tasks/abc170_d)
3. [Counting Divisors](https://cses.fi/problemset/task/1713)
4. [Modified GCD](https://codeforces.com/problemset/problem/75/C)
5. [Sum of Divisors](https://cses.fi/problemset/task/1082).  Hint: separar los casos por debajo de $`O(\sqrt{n})`$ de los que están por encima.
6. [Divisor Analysis](https://cses.fi/problemset/task/2182)


# Prime Numbers
Los números primos son números enteros positivos que tienen exactamente dos divisores distintos: 1 y el mismo número.  Si un número tiene más de 2 divisores, se dice que es compuesto.  El 0 y el 1 no son considerados primos ni compuestos. Anécdota: hasta el siglo XIX, 1 se consideraba primo.


## Primality Test
Consiste en determinar si un número entero **_x_** es primo o no.


### Trial Division
Podemos comprobar si un número **_x_** es primo o no, ejecutando un loop similar al de encontrar divisores: si aparece un divisor mayor que 1 (y menor que **_x_**), el número no es primo.

```c++
bool is_prime(int x) {
   for (int d = 2; d*d <= x; d++)
      if (x % d == 0)  // d es divisor de x => x no es un primo
         return false;
   return true;
}
```

Complejidad: $`O(\sqrt{x})`$


### Trial Division with Precomputed Primes
Si tenemos precomputado una lista de primos (con valores hasta la raiz cuadrada de **_x_**), podemos iterar dicha lista para encontrar posibles divisores.  Esto se debe a que **_x_** es divisible por un divisor compuesto, **_x_** también es divisble por uno de los factores primos de dicho divisor compuesto.

```c++
bool is_prime(int x) {
   // primes es una lista de numeros primos
   for (int i = 0; primes[i] * primes[i] <= x; i++) {
      if (x % primes[i] == 0)  // primes[i] es divisor de x => x no es un primo
         return false;
   }
   return true;
}
```

Complejidad: $`O(\sqrt{x / ln(x)})`$


### Miller-Rabin
TODO


### Ejercicio(s) de práctica:
1. [Prime String](https://www.hackerearth.com/practice/math/number-theory/primality-tests/practice-problems/algorithm/primestring/)
2. [Does it divide](https://www.hackerearth.com/practice/math/number-theory/primality-tests/practice-problems/algorithm/does-it-divide-3c60b8fb/)


## Cantidad de Numeros Primos
Existe una cantidad infinita de números primos.

La [cantidad de números primos hasta **_x_**](https://en.wikipedia.org/wiki/Prime_number#Number_of_primes_below_a_given_bound) se estima como $`x / ln(x)`$.

La maxima distancia entre dos numeros primos consecutivos hasta **_x_** va aumentando mientras mas grande sea **_x_**.  Aun asi, debido a la alta cantidad de numeros primos, esta distancia (también llamado prime gap) es relativamente pequeña.  Por ejemplo, hasta $`x = 10^9`$, el prime gap es de apenas 282.


## Sieve of Eratosthenes
Este eficiente algoritmo, del matemático griego Eratóstenes (siglo III AC), precalcula la primalidad de números hasta un tope $N$ en complejidad $`O(N \cdot log(log(N)))`$. El algoritmo empieza asumiendo que cada número entero (a partir de 2) es primo.  Por cada número primo que detecta, el algoritmo descarta los múltiplos de dicho número primo.


```cpp
const int N = 1'000'000;
bool is_prime[N+1];
vector<int> primes;
void sieve() {
   for (int n = 2; n <= N; ++n)
      is_prime[n] = true;
   for (int n = 2; n <= N; ++n) {
      if (is_prime[n]) {
         for (int m = 2*n; m <= N; m += n)
            is_prime[m] = false;
      }
   }
   for (int n = 2; n <= N; ++n)
      if (is_prime[n])
         primes.push_back(n);
}
```
Complejidad: $`O(N \cdot log(log(N)))`$


### $`\sqrt{N}`$ Trick

En el algoritmo, todos los múltiplos del primo **_n_** menores que $`n^2`$ ya fueron descartados cuando se procesaron los primos mas pequeños que **_n_**.  Por lo tanto, el loop interno puede empezar a partir de $`n^2`$.

```cpp
const int N = 1'000'000;
bool is_prime[N+1];
vector<int> primes;
void sieve() {
   for (int n = 2; n <= N; ++n)
      is_prime[n] = true;
   for (int n = 2; n <= N; ++n) {
      if (is_prime[n]) {
         for (long long m = n*1LL*n; m <= N; m += n)
            is_prime[m] = false;
      }
   }
   for (int n = 2; n <= N; ++n)
      if (is_prime[n])
         primes.push_back(n);
}
```
Complejidad: sigue siendo $`O(N \cdot log(log(N)))`$, pero en la práctica, reduce bastante el tiempo de ejecución.

### Bit Compression
Este truco es para ahorrar espacio de memoria si se desea preprocesar primos hasta **_N_** grandes. La idea es que es suficiente grabar un solo bit (en vez de un bool) por numero. En GNU C++, solo tenemos que reemplazar el arreglo **is_prime** por un ```vector<bool>```.

```cpp
const int N = 1'000'000;
vector<bool> is_prime(N+1);
```
Complejidad: sigue siendo igual, tanto para tiempo como espacio.  En la práctica, el espacio se reduce en un factor de 8 (porque hay 8 bits en un byte) y el tiempo de ejecucion tambien se reduce por su efecto en el cache del sistema.

### Least Prime
En vez de grabar un bool por número, grabamos el primo mas pequeño que divide al número.

```cpp
const int N = 1'000'000;
int least_prime[N+1];
vector<int> primes;
void sieve() {
   least_prime[0] = least_prime[1] = -1;
   for (int n = 2; n <= N; ++n)
      least_prime[n] = n;   // Inicialmente asumimos que cada n es el "primo" mas pequeno que divide a n
   for (int n = 2; n <= N; ++n) {
      if (least_prime[n] == n) {
         for (long long m = n*1LL*n; m <= N; m += n)
            if (least_prime[m] > n)
               least_prime[m] = n;
      }
   }
   for (int n = 2; n <= N; ++n)
      if (least_prime[n] == n)
         primes.push_back(n);
}
```
Complejidad: $`O(N \cdot log(log(N)))`$

### Linear Time
Es posible reducir la complejidad del algoritmo anterior a $`O(N)`$, pero no es intuitivo de porque tiene esa complejidad. La idea es reemplazar el loop interno por un loop de los números primos encontrados anteriormente.  En adición, dicho loop interno se ejecuta aunque **_n__* es compuesto.  Para una mejor explicación, leer el [artículo en cp-algorithms.com](https://cp-algorithms.com/algebra/prime-sieve-linear.html).

```cpp
const int N = 1'000'000;
int least_prime[N+1];
vector<int> primes;
void sieve() {
   least_prime[0] = least_prime[1] = -1;
   for (int n = 2; n <= N; ++n)
      least_prime[n] = n;   // Inicialmente asumimos que cada n es el "primo" mas pequeno que divide a n
   for (int n = 2; n <= N; ++n) {
      if (least_prime[n] == n)
         primes.push_back(n);
      for (int p : primes) {
         if (p > least_prime[n] || n * p > N) break;
         least_prime[n * p] = p;
      }
   }
}
```
Complejidad: $`O(N)`$

### Ejercicio(s) de práctica:
1. [Micro and Prime Prime](https://www.hackerearth.com/practice/math/number-theory/primality-tests/practice-problems/algorithm/micro-and-prime-prime-1/)
2. [Roy and Shopping](https://www.hackerearth.com/practice/math/number-theory/primality-tests/practice-problems/algorithm/roy-and-shopping-20/)
3. [In Love with Primes](https://www.hackerearth.com/practice/math/number-theory/primality-tests/practice-problems/algorithm/in-love-with-primes/)
4. [Soldier and Number Game](https://codeforces.com/contest/546/problem/D)
5. [Sum of Divisors](https://atcoder.jp/contests/abc172/tasks/abc172_d)
6. [Happy New Year 2023](https://atcoder.jp/contests/abc284/tasks/abc284_d)
7. [Counting Divisors](https://cses.fi/problemset/task/1713)
8. [Simple Sum](https://www.codechef.com/problems/SMPLSUM)
9. [Square Pair](https://atcoder.jp/contests/abc342/tasks/abc342_d)
10. [Together Square](https://atcoder.jp/contests/abc254/tasks/abc254_d)


## Prime Factorization
El proceso de factorizar un número entero consiste en descomponer un número **_x_** entero positivo en sus factores primos.

El teorema fundamental de la aritmética dice que existe una sola manera de hacer esta descomposición (ignorando el orden de los factores).

Usualmente, escribimos la factorización de esta manera: $`x = 2^{e_2} \times 3^{e_3} \times 5^{e_5} \times \dots`$, donde los terminos con $`e_i = 0`$ se omiten.


### Trial Division
Similar a la extracción de divisores de un número, podemos iterar hasta la raíz cuadrada del numero:
```cpp
vector< pair<int,int> > factorize(int x) {
   vector< pair<int,int> > res;
   if (x <= 1)
      return {};
   for (int d = 2; d*d <= x; ++d) {
      int cnt = 0;
      for (; (x % d) == 0; x /= d)
         ++cnt;
      if (cnt > 0)
         res.emplace_back(d, cnt);
   }
   if (x > 1)
      res.emplace_back(x, 1);
   return res;
}
```
Complejidad: $`O(\sqrt{x})`$

### Trial Division by Precomputed Primes
Si tenemos una lista de primos (hasta la raíz cuadrada del número a factorizar), podemos iterar dicha lista en vez de iterar todos los divisores.

```cpp
vector< pair<int,int> > factorize(int x) {
   vector< pair<int,int> > res;
   for (int p : primes) {
      if (p*1LL*p > x) break;
      int cnt = 0;
      for (; (x % p) == 0; x /= p)
         ++cnt;
      if (cnt)
         res.emplace_back(p, cnt);
   }
   if (x > 1)
      res.emplace_back(x, 1);
   return res;
}
```
Complejidad: $`O(\sqrt{x / ln(x)})`$


### Least Prime Sieve
Si tenemos precomputado la criba de Least Prime hasta el número a factorizar, podemos usarla para factorizar en tiempo $`O(log(x))`$:
```cpp
vector< pair<int,int> > factorize(int x) {
   vector< pair<int,int> > res;
   while (x > 1) {
      int p = least_prime[x];
      if (res.empty() || res.back().first != p)
         res.emplace_back(p, 1);
      else
         res.back().second++;
      x /= p;
   }
   return res;
}
```
Complejidad: $`O(log(x))`$


### Pollard-Rho's

TODO


### Divisor Count
Con la factorización prima de un número **_x_**, podemos fácilmente contar la cantidad de divisores de **_x_**: es el producto de todos los exponentes + 1 en su factorización prima:

$`\displaystyle \prod_{i=1}^{k} (e_i + 1)`$


```cpp
int divisor_count(int x) {
   vector< pair<int,int> > factors = factorize(x);
   int res = 1;
   for (auto pf : factors)
      res *= (pf.second + 1);
   return res;
}
```
Complejidad: $`O(log(x))`$


### Ejercicios de práctica:
1. [Weakened Common Divisor](https://codeforces.com/problemset/problem/1025/B)
2. [Michael and Cryptography](https://acm.timus.ru/problem.aspx?space=1&num=2102)
3. [Queries About Numbers](https://www.codechef.com/problems/QNUMBER)
4. [Counting Divisors](https://cses.fi/problemset/task/1713)
5. [Square-free division](http://codeforces.com/contest/1497/problem/E1)


# GCD
Dado dos números **_a_** y **_b_**, el máximo común divisor (_greatest common divisor_ en inglés) se refiere al divisor más grande que divide a ambos números **_a_** y **_b_**.

Notas:
1) El máximo común divisor no puede ser negativo.  Por ejemplo, el máximo común divisor de $`a=-15`$ y $`b=-10`$ no es -5 porque 5 divide a ambos números y es mayor que -5.
2) Si uno de los números es cero, el maximo comun divisor es el otro número que no es cero.
3) si ambos números **_a_** y **_b_** son ceros, no existe un máximo comun divisor (tambien puedes decir que es infinito)

## Brute Force
Similar al algoritmo de fuerza bruta para obtener los divisores de un número, podemos iterar hasta el menor de los dos números

```cpp
int gcd(int a, int b) {
   for (int d = min(a, b); d > 1; --d)
      if (a % d == 0 && b % d == 0)
         return d;
   return 1;
}

```
Complejidad: $`O(min(a, b))`$


## GCD of prime factorization
GCD(a, b) es el producto de todos los factores primos elevado al menor exponente en la factorización prima de **_a_** y **_b_**.


## Euclidean Algorithm
Este algoritmo del siglo IV es considerado uno de los algoritmos (no triviales) más antiguos descubiertos/inventados. Su autor es el matemático griego Euclides.

```cpp
int gcd(int a, int b) {
   if (b == 0) return abs(a);
   else return gcd(b, a % b);
}
```
Complejidad: $`O(log(min(a, b)))`$


### Extended Euclidean Algorithm:
Un concepto relacionado al máximo común divisor es [Bézout's Identity](https://en.wikipedia.org/wiki/B%C3%A9zout%27s_identity), el cual dice que existen enteros números **_x_** y **_y_** que satisfacen la siguiente identidad:

$`a \cdot x + b \cdot y = gcd(a, b)`$

Para obtener esos valores **_x_** y **_y_**, podemos usar una variante del algoritmo de Euclides llamada _Extended Euclidean Algorithm_:

```cpp
int gcd(int a, int b, int& x, int& y) {
    if (b == 0) {
        x = 1;
        y = 0;
        return a;
    }
    int x1, y1;
    int d = gcd(b, a % b, x1, y1);
    x = y1;
    y = x1 - y1 * (a / b);
    return d;
}
```

Complejidad: $`O(log(min(a, b)))`$


Nota que la pareja $`(x, y)`$ obtenida por ese algoritmo es una de infinitas soluciones a la ecuación dada.  Pero todas las soluciones son de la forma:

$`(x + \frac{k \cdot b}{gcd(a, b)}, y - \frac{k \cdot a}{gcd(a, b)})`$

donde **_k_** es cualquier número entero.


### Diophantine Equations

Dado números enteros **_a_**, **_b_** y **_c_**, encuentra numeros enteros **_x_**, **_y_** que satisfacen la ecuación:

$`a \cdot x + b \cdot y = c`$

Existe solución si y solamente si $`gcd(a, b) \; | \; c`$.  En cuyo caso, una solución es:

$`(x' \cdot d, y' \cdot d)`$

donde:
* **_x'_**, **_y_** se obtienen con el [algoritmo extendido de Euclides](#extended-euclidean-algorithm).
* $`d = \frac{c}{gcd(a, b)}`$


## Ejercicios de práctica:
1. [Teleportation](https://atcoder.jp/contests/abc226/tasks/abc226_d)
2. [Sherlock and GCD](https://www.hackerrank.com/challenges/sherlock-and-gcd/problem)
3. [Common Divisors](https://codeforces.com/contest/1203/problem/C)
4. [Gcd Queries](https://www.codechef.com/problems/GCDQ)
5. [Weakened Common Divisor](https://codeforces.com/problemset/problem/1025/B)
6. [Modular GCD](https://www.codechef.com/problems/GCDMOD)
7. [GCD of an Array](https://codeforces.com/contest/1493/problem/D)


# LCM

El mínimo común múltiplo de **_a_** y **_b_** es el número entero positivo más que pequeño que es múltiplo de ambos números **_a_** y **_b_**.

## Brute Force
Iterar **_m_** hasta $`a \times b`$.  Devolver el mínimo **_m_** que sea múltiplo de **_a_** y **_b_**.

```cpp
long long lcm(int a, int b) {
   for (long long m = max(a, b); m <= a*1LL*b; ++m)
      if (m % a == 0 && m % b == 0)
         return m;
   assert(false);
}
```
Complejidad: $`O(a \times b)`$


## LCM of prime factorization
El LCM es el producto de todos los factores primos elevado al mayor exponente en la factorización prima de ambos números.


## Relation with GCD

$`lcm(a, b) = \frac{a \times b}{gcd(a, b)}`$

```cpp
long long lcm(int a, int b) {
   return a / gcd(a, b) * 1LL * b;
}
```
Complejidad: $`O(log(min(a, b)))`$


## Ejercicio de práctica:
1. [Orac and LCM](https://codeforces.com/contest/1350/problem/C)



----

