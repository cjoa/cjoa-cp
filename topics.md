## Asymptotic Complexiity

## STL
* Array / vector
* Queue / Stack
* Set / Map
* Priority Queue
* String

## Sorting
* Custom Comparator

## Binary Search
* Binary search in sorted container
  * lower_bound / upper_bound
* Binary search the answer
  * Discrete
  * Continuous (Bisection Method)
* Ternary Search

## Dynamic Programming (Basic)
* Subset Sum
* Knapsack
* Take or not Take
* Range DP
* Longest Increasing Subsequence
## Greedy

## Graph Theory
* Graph Traversal
  * DFS
    * Flood Fill
  * BFS
    * 0-1 BFS
* Identifying Connected Components
* Bipartite Check (Two-Coloring)
* Cycle detection
* Shortest Path
  * Single-Source
  * Multiple-Source
  * All-pair
* Topological Sorting
* Disjoint Set (Union-Find)
* Minimum Spanning Tree

## Number Theory
* Divisors
* GCD / LCM
* Sieve
* Modular Arithmetic

## Combinatorics
* Sum Rule
* Product Rule
* Inclusion-Exclusion
* Combinations
* Permutations

## Brute Force
* Generating Permutations
* Generating Combinations
* Generating Subsets
* Backtracking

## Miscellaneous
* Prefix Sum
* Sliding Window
* Two-Pointers
* Sweep Line

## Bitwise Operations

## Segment Tree
* With Lazy Propagation

## Square root Decomposition

## Meet in the Middle

## Dynamic Programming (Medium)
* Bitmask
* Digit DP
* Matrix Exponentiation
* With Segment Tree
* Tree DP

## Geometry
* Point
* Line
* Vector
* Triangle
* Polygon
* Circle

## Probability
* Expected Value

## Game Theory
* Grundy Number

## Graph Theory (Medium)
* Lowest Common Ancestor
  * Binary Lifting
* Strongly Connected Component
* Articulation Point / Bridge Finding
* Tree Linearization

## String (Medium)
* String Matching
  * Hashing
* Trie
