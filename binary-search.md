# Introduction to Binary Search

Una breve introducción a Busqueda Binaria

---


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Introduction](#introduction)
- [STL](#stl)
- [Implementation](#implementation)
- [Binary Search The Answer](#binary-search-the-answer)
- [Bisection Method](#bisection-method)
- [Ternary Search](#ternary-search)
- [Interactive](#interactive)


<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction

Binary Search es una tecnica basado en el paradigma de Divide and Conquer, donde el espacio de busqueda se reduce a la mitad en cada iteracion.

Los ejemplos clasicos de binary search son:
* Buscar un numero en un arreglo _ordenado_.
* Adivinar un numero entero secreto entre 1 a 100 donde cada iteracion te permite preguntar por un numero y te responde si ese numero es mayor, igual, o menor que el numero secreto.

Otros recursos
* [Codeforces ITMO Academy Pilot Course](https://codeforces.com/edu/course/2/lesson/6)



## STL

### Funcion binary_search
```c++
vector<int> V = {2, 3, 3, 5, 7, 8, 8, 8, 10};
cout << binary_search(V.begin(), V.end(), 7) << endl;
cout << binary_search(V.begin(), V.end(), 3) << endl;
cout << binary_search(V.begin(), V.end(), 9) << endl;
```

### Funciones lower_bound y upper_bound
```c++
vector<int> V = {2, 3, 3, 5, 7, 8, 8, 8, 10};
cout << lower_bound(V.begin(), V.end(), 7) - V.begin() << endl;
cout << lower_bound(V.begin(), V.end(), 3) - V.begin() << endl;
cout << lower_bound(V.begin(), V.end(), 9) - V.begin() << endl;

cout << upper_bound(V.begin(), V.end(), 7) - V.begin() << endl;
cout << upper_bound(V.begin(), V.end(), 3) - V.begin() << endl;
cout << upper_bound(V.begin(), V.end(), 9) - V.begin() << endl;
```

### Ejercicios de Practica
1. [Interesting Drink](https://codeforces.com/problemset/problem/706/B/)
2. [Counting Haybales](https://usaco.org/index.php?page=viewproblem2&cpid=666)
3. [Math is Love](https://www.spoj.com/problems/MATHLOVE/)
4. [Subarray Sums I](https://cses.fi/problemset/task/1660)


## Implementation

```c++
int binary_search(int lo, int hi) {
   int res = -1; // remains -1 if check(mid) never yields true
   while (lo <= hi) {
      int mid = lo + (hi - lo) / 2;
      if (check(mid)) {
         hi = mid - 1;
         res = mid;     // if searching for last FALSE, move this line to else case  
      } else {
         lo = mid + 1;
      }
   }
   return res;
}
```


```c++
int binary_search(int lo, int hi) {
   lo--, hi++;
   while (hi - lo > 1) {
      int mid = lo + (hi - lo)/2;
      if (check(mid))
         hi = mid;
      else
         lo = mid;
   }
   return hi; // check(lo) == false and check(hi) == true
}
```


## Binary Search The Answer


### Ejercicios de Practica
1. [Factory Machines](https://cses.fi/problemset/task/1620)
2. [Copying Books](https://www.spoj.com/problems/BOOKS1/)
3. Alternativa, [Array Division](https://cses.fi/problemset/task/1085)
4. [Aggressive Cows](https://www.spoj.com/problems/AGGRCOW/)
5. [Maximum Median](https://codeforces.com/problemset/problem/1201/C)
6. [Multiplication Table](https://cses.fi/problemset/task/2422)


## Bisection Method

```c++
double binary_search_continuous(double lo, double hi) {
   for (int t = 1; t <= 100; ++t) {
      double mid = lo + (hi - lo)/2;
      if (check(mid))
         hi = mid;
      else
         lo = mid;
   }
   return lo;            // lo is close to the border between no and yes
}
```

### Ejercicios de Practica
1. [Pie](https://www.spoj.com/problems/PIE/)
2. [Energy exchange](https://codeforces.com/contest/68/problem/B)
3. [Water Bottle](https://atcoder.jp/contests/abc144/tasks/abc144_d)



## Ternary Search

```c++
double ternary_search(double L, double R) {
   for (int t = 0; t < 200; ++t) {
      double L3 = (L*2 + R) / 3;
      double R3 = (L + R*2) / 3;
      if (F(L3) < F(R3))
         L = L3;
      else
         R = R3;
   }
   return (L+R)/2;
}
```



## Interactive

En problemas interactivos, usualmente piden que descubras una informacion secreta escondida, utilizando operaciones dadas pero limitadas a una cantidad de veces que puedes usarlas.

### Ejercicios de Practica
1. [Guess the Number](https://codeforces.com/gym/101021/problem/1)
2. [Glad to see you!](https://codeforces.com/contest/810/problem/D)
3. [Mahmoud and Ehab and the binary string](https://codeforces.com/contest/862/problem/D)
4. [Cave](https://oj.uz/problem/view/IOI13_cave)

