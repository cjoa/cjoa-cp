* Integer overflow
* Off-by-one indices
* INF values set too small
* Floating point precision
* Uninitialized variables
* Global variables not reset after each test case
* Passing large data structures by value to a function
* Confusing first vs second in a pair
