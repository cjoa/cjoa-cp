# Combinatorics

Una breve introducción a Combinatorica para Programación Competitiva

Otros tutoriales:
* [Counting](https://ggc-discrete-math.github.io/counting.html)
* [Brilliant](https://brilliant.org/wiki/combinatorics/)
* [TopCoder](https://www.topcoder.com/thrive/articles/Basics%20of%20Combinatorics)

---


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Rule of Product](#rule-of-product)
- [Rule of Sum](#regla-del-producto)
- [Complement](#complement)
- [Principle of Inclusion-Exclusion](#principle-of-inclusion-exclusion)
- [Pigeonhole Principle](#pigeonhole-principle)
- [Permutation](#permutation)
- [Combination](#combination)
- [Subset](#subset)


<!-- END doctoc generated TOC please keep comment here to allow auto update -->



## Rule of Sum
[Wikipedia](https://en.wikipedia.org/wiki/Addition_principle)

Utilizas esta regla para contar varios conjuntos de objetos, donde cada conjunto es de distinto tipo (o sea, que no se _solapan_).

Ejemplo: Si tienes 5 manzanas y 7 guineos, tienes $`5 + 7`$ frutas en total.


## Rule of Product
[Wikipedia](https://en.wikipedia.org/wiki/Rule_of_product)

Utilizas esta regla para contar la cantidad de maneras de ejecutar una tarea, donde la tarea la puedes descomponer en pasos.

Ejemplo: Si el almuerzo te ofrecen 5 platos fuertes y 3 postres, tienes $`5 \times 3`$ maneras de almorzar.


## Complement
A veces es mas simple contar los elementos que estan _ausentes_ de un conjunto y luego substraerlos del total.

Ejemplo: Cuantos strings de 3 letras minusculas hay que tienen al menos una letra 'a'?


### Ejercicio de Practica
1. [Kolya and Tanya](https://codeforces.com/contest/584/problem/B)


## Principle of Inclusion-Exclusion
[Brilliant](https://brilliant.org/wiki/principle-of-inclusion-and-exclusion-pie/)


$`\displaystyle |A \cup B| = |A| + |B| - |A \cap B|`$

$`\displaystyle |A \cup B \cup C| = |A| + |B| + |C| - |A \cap B| - |A \cap C| - |B \cap C| + |A \cap B \cap C|`$



### Ejercicios de Practica
1. [Insomnia cure](https://codeforces.com/contest/148/problem/A)



## Pigeonhole Principle
[Brilliant](https://brilliant.org/wiki/pigeonhole-principle-definition/)


## Permutation

Reordenar todos los elementos de un conjunto.

$`\displaystyle P(n) = n!`$


Ejemplo: las permutaciones del conjunto {1, 2, 3} son:
* (1, 2, 3)
* (1, 3, 2)
* (2, 1, 3)
* (2, 3, 1)
* (3, 1, 2)
* (3, 2, 1)


### _k_-Permutation

Seleccionar **_k_** elementos de un conjunto de **_n_** elementos, donde el orden de seleccion importa.

$`\displaystyle P(n, k) = \frac{n!}{(n-k)!}`$


Ejemplo: en un club con 10 miembros, cuantas maneras hay de elegir un presidente, un vicepresidente y un tesorero?
$`10 \cdot 9 \cdot 8`$


### Circular Permutation

La cantidad de permutaciones de **_n_** objetos que estan alrededor de un circulo es $`(n-1)!`$.  Esto se debe a que cada permutation original genera **_n_** permutaciones que son equivalentes (en un circulo).  Por lo tanto, la respuesta es $`\frac{n!}{n}`$.



### Derangement

TODO



### Permutation Cycle

TODO



## Combination

Seleccionar **_k_** elementos de un conjunto de **_n_** elementos, donde el orden de seleccion no importa.  Aqui se asume que no se permiten repeticiones.

$`\displaystyle \binom{n}{k} = \frac{n!}{k!(n-k)!}`$


### Combination with Repetition

Seleccionar **_k_** elementos de un conjunto de **_n_** elementos donde se permiten repeticiones y el orden de seleccion no importa.

- [LibreTexts](https://math.libretexts.org/Courses/Monroe_Community_College/MTH_220_Discrete_Math/7%3A_Combinatorics/7.5%3A_Combinations_WITH_Repetitions)
- [Wikipedia](https://en.wikipedia.org/wiki/Combination#Number_of_combinations_with_repetition)


Considera el siguiente otro problema: cuantas maneras hay de colocar **_k_** pelotas identicas en **_n_** urnas distintas?

Resulta que ambos estan contando lo mismo:
1. Considera los **_n_** elementos del problema original como _urnas_
2. Seleccionar **_k_** de ellos equivale a "colocar **_k_** pelotas en las urnas"


Una tecnica muy util para resolver esos problemas se llamada [Stars and Bars](https://en.wikipedia.org/wiki/Stars_and_bars_(combinatorics)).  La idea es colocar las urnas en una linea y utilizar _barras_ para separar dos urnas adyacentes.  Las pelotas se colocarian entre las barras.  Por ejemplo, estas son algunas maneras de colocar 5 pelotas en 3 urnas: 

```
*|**|*
***||**
|*|****
|*****|
```

La cantidad de maneras es igual a la cantidad de maneras de colocar **_k_** estrellas y **_n - 1_** barras en un string de tamaño **_k + n - 1_**:

$`\displaystyle \binom{k + n - 1}{n - 1}`$


Que pasa si no se permite dejar una urna vacia?  Bueno, forzamos una pelota en cada urna:

$`\displaystyle \binom{k - 1}{n - 1}`$


Otro problema similar: Cuantas soluciones existen para la ecuacion $`x_1 + x_2 + x_3 = 5`$, donde las variables $`x_1`$, $`x_2`$ y $`x_3`$ son enteras no negativas?


### Ejercicios de practica
1. [Duodecim Ferra](https://atcoder.jp/contests/abc185/tasks/abc185_c)
2. [How do you add?](https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=1884)
3. [MARBLES](https://www.spoj.com/problems/MARBLES/)
4. [Redistribution](https://atcoder.jp/contests/abc178/tasks/abc178_d)
4. [Two Arrays](https://codeforces.com/contest/1288/problem/C)
5. [One-Dimensional Puzzle](https://codeforces.com/contest/1931/problem/G)


### Hockey Stick Identity

TODO



## Summary


| nombre                        | ordenado? | repeticiones? |                              |
|-------------------------------|-----------|---------------|------------------------------|
| k-tuples                      |    si     |     si        | $`n^k`$                      |
| k-permutations                |    si     |     no        | $`\frac{n!}{(n-k)!}`$        |
| k-combinations                |    no     |     no        | $`\binom{n}{k}`$             |
| k-combinations w/repetitions  |    no     |     si        | $`\binom{n + k - 1}{k - 1}`$ |



## Subset

Seleccionar algunos (o ningun) elementos de un conjunto de **_n_** elementos.  El orden de seleccion no importa.

La cantidad total de subconjuntos de un conjunto de **_n_** elementos es $`2^n`$.

Nota que:

$`\displaystyle \sum_{k=0}^{n} \binom{n}{k} = 2^n`$


Las subsecuencias de una secuencia tambien se pueden considerar como subconjuntos.


## Integer Partition

TODO


## Catalan Number

TODO


