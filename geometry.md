# Geometry

Una breve introducción a Geometría para Programación Competitiva

---


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Puntos](#points)
- [Lineas y Segmentos](#lines-segments-and-rays)
- [Triangulos](#polygons-triangles)
- [Rectangulos](#poligons-rectangles)
- [Circulos](#circles)
- [Sweep Line](#sweep-line)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Points

Los puntos pueden tener varias dimensiones.  En programacion competitiva, es comun trabajar con dos dimensiones.

### Representation

```c++
struct Point {
   int x, y;   // Reemplazar int por el tipo de datos que vas a trabajar
};
```

### Distance
#### Manhattan Distance

Distancia Manhattan (tambien llamado "Taxi-Cab Distance"), entre dos puntos $`p = (x_p, y_p)`$ y $`q = (x_q, y_q)`$:

$`|x_p - x_q| + |y_p - y_q|`$

```c++
long long manhattan_distance(Point p, Point q) {
   return abs( p.x - q.x ) + abs( p.y - q.y );
}
```

#### Euclidean Distance

Distancia Euclideana entre dos puntos $`p = (x_p, y_p)`$ y $`q = (x_q, y_q)`$:

$`\sqrt{ (x_p - x_q)^2 + (y_p - y_q)^2 }`$

```c++

double euclidean_distance(Point p, Point q) {
   return sqrt( (p.x - q.x) * 1LL * (p.x - q.x) +
                (p.y - q.y) * 1LL * (p.y - q.y) );
}
```

Nota que la distancia euclideana se puede extender a puntos en tres o mas dimensiones.


## Lines, Segments and Rays

* Segmento tiene un punto de inicio y un punto fin.
* Rayo tiene un punto de inicio, pero no tiene un punto fin.
* Lineas no tiene ni inicio ni fin.

### Representation

#### General Equation of a Line

La ecuacion de la linea se escribe de esta manera: $`A \cdot x + B \cdot y + C = 0`$.

```c++
struct Line {
   double A, B, C;
};
```

#### Line that passes through two points

```c++
struct Line {
   Point a, b;
};
```

#### Line in Slope-Intercept Form

Escribimos la ecuacion de la linea de esta manera: $`f(x) = m \cdot x + b`$, donde
$`m`$ es la pendiente y $`b`$ en el punto de interseccion de la linea con el
eje $`Y`$.

```c++
struct Line {
   double m, b;
};
```


#### Line in Point-Slope form

Escribimos la ecuacion de la linea de esta manera: $`f(x) = m \cdot (x-x_1) + y_1`$,
donde $`m`$ es la pendiente y $`(x_1, y_1)`$ es un punto por donde la linea pasa.

```c++
struct Line {
   double m, x1, y1;
};
```


#### Vector

Primero definamos un vector:

```c++
using Vector = Point;
```

##### Vector Magnitude

$`|\textbf{v}| = \sqrt{x_v^2 + y_v^2}`$


```c++
double magnitude(Vector v) {
   return std::sqrt( v.x * 1LL * v.x + v.y * 1LL *v.y );
};
```



##### Cross Product
El producto cruz de los vectores **_u_** y **_v_** es:

$`|\textbf{u} \times \textbf{v}| = x_u \cdot y_v - x_v \cdot y_u`$

```c++
long long cross_product(Vector u, Vector v) {
   return u.x * 1LL * v.y - u.y * 1LL * v.x;
}
```

Formula alterna del producto cruz:
$`|\textbf{u} \times \textbf{v}| = |\textbf{u}||\textbf{v}| \cdot sin \theta`$


##### Dot Product
El producto punto de los vectores **_u_** y **_v_** es:

$`\textbf{u} \cdot \textbf{v} = x_u \cdot x_v + y_u \cdot y_v`$


```c++
long long dot_product(Vector u, Vector v) {
   return u.x * v.x + u.y * v.y;
}
```


Formula alterna del producto punto:
$`\textbf{u} \cdot \textbf{v} = |\textbf{u}||\textbf{v}| \cdot \cos \theta`$


### Orientation of a point with respect to a line

Dado puntos **_a_**, **_b_** y **_p_**, el signo del producto cruz del vector entre los puntos **_a_** y **_b_** y el vector entre **_a_** y **_p_** te dice de cual _lado_ se encuentra el punto **_p_** con respecto al rayo que va de **_a_** y **_b_**:
* Si es positivo, el punto **_p_** se encuentra a la izquierda del rayo **_a_** y **_b_**.
* Si es negativo, el punto se encuentra a la derecha del rayo **_a_** y **_b_**.
* Si es cero, la linea que pasa por los puntos **_a_** y **_b_** tambien atraviesa el punto **_p_**. Es decir, el punto **_p_** es colinear con la línea.


#### Ejercicios de Practica
1. [Collinearity](https://atcoder.jp/contests/abc181/tasks/abc181_c)
2. [Point Location Test](https://cses.fi/problemset/task/2189)
3. [Line Segment Intersection](https://cses.fi/problemset/task/2190)
4. [Convex Quadrilateral](https://atcoder.jp/contests/abc266/tasks/abc266_c)


### Line Intersection

Despejar variables en cada ecuacion de líneas y luego igualar variables.


#### Determine whether two lines intersect?

TODO.


#### Determine whether two segments intersect?


TODO.


### Angle between two lines

TODO.


### Distance of a point to a line


TODO.


## Polygons: Triangles

### Area:

#### Classic Formula
$`\frac{1}{2} \cdot b \cdot h`$


#### Heron's Formula

$` \sqrt{ s \cdot (s - a) \cdot (s - b) \cdot (s - c) } `$

donde:
$`s = \frac{a + b + c}{2}`$


#### Cross Product Formula

Dado tres puntos **_a_**, **_b_** y **_c_**, el area del triangulo con vertices en los puntos **_a_**, **_b_** y **_c_**) es dado por la formula:

$`\frac{1}{2} \cdot | \textbf{u} \times \textbf{v} |`$

donde:
**_v_** es el vector que va de **_a_** a **_c_**.
**_u_** es el vector que va de **_a_** a **_b_**.


### Pythagorean Theorem

$`h^2 = a^2 + b^2`$

donde:
* **_a_** y **_b_** son las longitudes de los catetos
* **_h_** es la longitud de la hipotenusa.


### Law of Cosines

$`c^2 = a^2 + b^2 - 2 \cdot a \cdot b \cdot \cos \gamma`$

Tener en cuenta que el angulo debe ser en radianes.


### Triangle Inequality

En todo triangulo (con area no cero) con lados **_a_**, **_b_** y **_c_**, donde $`a \leq b \leq c`$, se cumple que $`a + b > c`$.

## Polygons: Rectangles

Area:
$`b \cdot h`$


## Area of a polygon


## Circles

Area:
$`\pi \cdot r^2`$


### Circumcircle

* [Wikipedia](https://en.wikipedia.org/wiki/Circumcircle)


----



## Sweep Line

* [Competitive Programmer's Handbook](https://usaco.guide/CPH.pdf#page=286)

```c++
struct Evento {   // == Punto interesante
   char tipo;  // E = Entrada   S = Salida
   int x, id;
   bool operator<(const Evento& e) const {
      if (x != e.x)
         return x < e.x;
      // x son iguales
      return tipo < e.tipo;
   }
};

int main() {
   // leer vector< pair<int,int> > intervalos

   // Agregar los eventos
   vector<Evento> eventos;
   for (int i = 0; i < N; i++) {
      auto [L, R] = intervalos[i];
      eventos.push_back({ 'E', L, i });
      eventos.push_back({ 'S', R, i });
   }

   // Ordenar eventos por x, tipo
   sort(eventos.begin(), eventos.end());

   // Procesar los eventos
   for (Evento e : eventos) {
      if (e.tipo == 'E') {  // Evento de entrada
      }
      else {  // Evento de salida
      }
   }
}

```

#### Ejercicios de Practica
1. [Restaurant Customers](https://cses.fi/problemset/task/1619)
2. [Room Allocation](https://cses.fi/problemset/task/1164)
